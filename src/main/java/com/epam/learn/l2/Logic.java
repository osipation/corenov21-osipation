package com.epam.learn.l2;

public class Logic {
    private boolean paramA = true;
    private boolean paramB = false;
    private boolean paramC = true;

    public static void main(String[] args) {
        Logic logic = new Logic();
        System.out.println(logic.checkResultFirst(logic.paramA, logic.paramB, logic.paramC));
    }

    private boolean checkResultFirst(boolean a, boolean b, boolean c) {
        return a || c;
    }
}
